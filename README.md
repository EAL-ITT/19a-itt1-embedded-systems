![Build Status](https://gitlab.com/EAL-ITT/19a-itt1-embedded-systems/badges/master/pipeline.svg)


# 19A-ITT1-embedded-systems

weekly plans, resources and other relevant stuff for the embedded systems lectures in IT technology 1. semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt1-embedded-systems/)