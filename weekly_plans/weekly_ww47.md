---
Week: 47
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 47 ITT1 Embedded Systems

## Goals of the week(s)
Project Work. An simple embedded project that connects buttons, a sensor, a joystick and an actuator will run for the rest of the semester.

### Practical goals

1. Create a motor control software with PWM via an H-bridge.
2. Interface buttons to start and stop the motor.
3. Interface buttons to start and stop the software.

### Learning goals

1. Interfacing sensors, actuators, analog input and digital inputs.


## Deliverable

1. Motor and H-bridge diagram.
2. Software.
3. Button interface diagrams.
4. This is an alternative delivery for week 41.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
Use knowledge from previews weeks. This mini project will be part of your first semester exam.

# Mandatory Assignment - Second Attempt

Present a PDF report with documented work from week 45 and 46. Students will need to:

* Control a LED with PWM, represented by pictures, code snips, and oscilloscope output.
* Students will need to have hand a diagram with the link of PI to a H-Bridge to a DC Motor
