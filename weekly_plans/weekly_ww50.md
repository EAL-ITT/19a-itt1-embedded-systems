---
Week: 50
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 50 ITT1 Embedded Systems

## Goals of the week(s)
Project Work. An simple embedded project that connects buttons, a sensor, a joystick and an actuator will run for the rest of the semester.

### Practical goals

1. Create a motor control software with PWM via an H-bridge.
2. Interface buttons to start and stop the motor.
3. Interface buttons to start and stop the software.
4. Implement TCP/IP with sockets. Server Client application between RPI and you laptop.
5. Using firebase database, right the values of the CPU temperature of the RPI. Remotely start the DC Motor using sockets and controlling the PWM.
6. Using a second set of laptop/RPI send temperature of CPU over the Database, and calculate the PWM rate "offsite". Send back the PWM value to start the motor.

### Learning goals

1. Interfacing sensors, actuators, analog input and digital inputs.



## Deliverable

1. Motor and H-bridge diagram.
2. Software.
3. Button interface diagrams.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
