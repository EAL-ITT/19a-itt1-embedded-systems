---
Week: 51
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 51 ITT1 Embedded Systems

## Goals of the week(s)
After some student asked I have updated this week. Below a summery of all tasks is given, as well as some extra activities for the students that are already done.

### Practical goals

1. Finish tasks from earlier weeks.
2. ADC controller fan with PWM
3. Temperature controlled fan with PWM. Students need to read the CPU temperature value and start a fan at given temperature. Temperature start should be around 50 degrees C, before thermal throttling occurs.
4. Data of temperature should be send via TCP/IP using sockets from the RPI to the workstation. Workstation will return the value of PWM value in order to engage the fan.
5. Data should be forward from workstation to an online database, use Firebase from google.
6. Data should forward from database to another workstation, and from there to a second RPI.
7. Students should end up having temperature measurement on RPI, send data to workstation using sockets, forward the data to Firebase. Firebase will allow a second workstation to read the data, workstation will forward data to a second RPI. The second PI will calculate the PWM value and send it back to the original PI in order to engage the fan.
8. Make network drawings.
9. Make logic diagram how your software works.
10. Make a long youtube video and send it to RUTR
11. Build a quick website to show the results online - hint take data directly from the Database.


### Learning goals

1. IoT technologies.
2. Build an IoT network based on a simplified industry example.
3. Applying knowledge from other courses, for example programming and networking.
4. Real time communication and distributed work load and tasks.


## Deliverable

1. Video on Youtube or another platform.

## Schedule

See Time Edit

## Hands-on time

Please hand no later than first lecture in January.

## Comments

Not all the tasks can be solved by everyone. Students should focus on what they can accomplish. The base line is marked as practical goal 4. It is expected that everyone will accomplish at least to this point. Students then should build progressively and challenge themselves with the next tasks. For the students that have complied all the tasks there is a special surprise.
