---
Week: 43
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 43 ITT1 Embedded Systems

## Goals of the week(s)
Students will be introduced to computer architecture. Basic concept such as CPU, Memory, Registers, Operating Systems, and Kernel will be presented.

### Practical goals

1. Students will be asked to create a simple software that will read CPU load.
2. Students will be asked to create a simple software that will read Memory.
3. Students will be asked to check disk space.
4. Everything has to be in bash, no command line.
5. Use file system, tools and navigate around your project file and dump files.
6. The commands need to be dumped in a file. The output recorded and manipulated with more advanced commands, aka, not only top command. But also use the extensions.
7. Script your way around, not easy command.


### Learning goals

1. BASH scripting.
2. Using BASH identify system parameters and peripheral.
3. Bash and navigation.
4. File system - linux.
 


## Deliverable

1. Students will deliver the output of the du, df free and w commands.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
N/A
