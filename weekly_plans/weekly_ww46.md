---
Week: 46
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 46 ITT1 Embedded Systems

## Goals of the week(s)
Due to still missing components, this weeks goals changed as following.

### Practical goals

1. Students will use matlab to simulate, DC motor, mechanically and electrically
2. Students will use matlab to simulate DC motor driven by an H-bridge or a motor drive board.
2. Students will learn about steady state, rise time and the other aspects of response in DC motors.

### Learning goals

1. Implement of simulations using matlab


## Deliverable

1. Hand in Gitlab, software.
2. Hand in Gitlab, oscilloscope output, from matlab's scope

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
Links for matlab tutorials. Links are not in any specific order.

https://www.mathworks.com/help/physmod/sps/examples/pwm-controlled-dc-motor.html

https://www.mathworks.com/videos/modeling-a-dc-motor-68852.html

https://www.mathworks.com/videos/how-to-design-motor-controllers-using-simscape-electrical-part-1-simulating-back-emf-voltage-of-a-bldc-motor-1565241566392.html

https://www.mathworks.com/videos/motor-control-part-2-bldc-motor-control-1567761327790.html

https://www.mathworks.com/videos/brushless-dc-motors-introduction-1564728874059.html
