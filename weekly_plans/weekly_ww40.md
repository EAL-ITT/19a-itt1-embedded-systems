---
Week: 39 - 40
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 39 and Week 40 ITT1 Embedded Systems

## Goals of the week(s)

Week 39 and Week 40 share goals, due to study trip. 

### Practical goals

1. Students will implement a analog to digital conversion simple system. Week `39 - 40`
2. Students will measure a voltage power-supply and read them into the raspberry pi. Week `39 - 40`
3. Students will measure the voltage from a battery pack - power bank. Week `39 - 40`

### Learning goals

1. Analog to Digital signal conversion.
2. The MCP3008.


## Deliverable

1. Hand in Gitlab, the python code.
2. Hand in Gitlab, circuit design.
3. Hand in Gitlab, pictures of your set up and a working solution.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverables.

## Links

See week 39

### SPI

https://www.youtube.com/watch?v=ba0SQwjTQfw

https://www.youtube.com/watch?v=DvuwhlREi4U

## Comments
We will see if the class is pressed we can work on this again on a later date. Due to the study trip there is a divination of one working day.
