---
Week: 45
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 45 ITT1 Embedded Systems

## Goals of the week(s)
Introduction of DC and AC motors. The students will be presented with the theory behind DC motors and H-Bridges. A simple introduction to AC motors.

### Practical goals

1. Students will try to blink a LED with PWM
2. Students will create a pulse modulation. With the help of the oscilloscope they will verify frequency and oscillation.
3. Students will document their finding and be ready to implement it on a h-bridge next week. 

### Learning goals

1. Simple Motor Theory.
2. Simple H-Bridge and control theory.
3. Pulse Width Modulation, implemented.


## Deliverable

1. Hand in Gitlab, software.
2. Hand in Gitlab, oscilloscope output. 

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments - Links
We are missing some components, I am ordering today - 04/11/2019. They should be here start of next week.

https://www.sparkfun.com/datasheets/Robotics/TB6612FNG.pdf

https://www.modularcircuits.com/blog/articles/h-bridge-secrets/h-bridges-the-basics/

http://www.mcmanis.com/chuck/robotics/tutorial/h-bridge/index.html

https://www.instructables.com/id/Raspberry-PI-L298N-Dual-H-Bridge-DC-Motor/

https://www.youtube.com/watch?v=AMFp52o4uRA

https://www.youtube.com/watch?v=2bganVdLg5Q

https://circuitdigest.com/microcontroller-projects/raspberry-pi-pwm-tutorial

https://www.youtube.com/watch?v=G6EaI1RL42I

### Motor Theory

https://www.orientalmotor.com/brushless-dc-motors-gear-motors/technology/AC-brushless-brushed-motors.html

https://www.youtube.com/watch?v=LAtPHANEfQo

https://www.youtube.com/watch?v=bCEiOnuODac

https://www.youtube.com/watch?v=ZAY5JInyHXY

https://www.youtube.com/watch?v=Jz0fsCPZCNY

https://www.youtube.com/watch?v=AQqyGNOP_3o

https://www.youtube.com/watch?v=DsVbaKZZOFQ
