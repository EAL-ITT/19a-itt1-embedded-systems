---
Week: 44
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 44 ITT1 Embedded Systems

## Goals of the week(s)
Overclocking Raspberry PI. A prime calculation algorithm will be implemented. the RPI will be pushed to the limit.

### Practical goals

1. Students will be asked to write a software that will calculate prime numbers.
2. The RPI will have to be over clocked and then the test is executed again.
3. Students will have to do the prime number calculation while read analog values from a potentiometer.

### Learning goals

1. Understand of hardware limitations.
2. Understanding of voltages damaging effects on a circuit.


## Deliverable

1. Students will have to document analog input from voltage sources. During prime number calculation, with and without overclocked CPU.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments

Below is a list of useful link and information

### Prime Numbers

https://whatis.techtarget.com/definition/prime-number

#### youtube.

https://www.youtube.com/watch?v=lz8Hya8SHHI

Note that if you want to print the number on the screen do not. As print takes more execution cycles. So your printing should be kept to minimal, only print the actual prime number.

### Over clocking - At your own risk

Students, can - can overcloack. They are not asked to do it. By running prime number calculation, students will notice that their RPI are been pushed to the limit, simple instructions will not be possible any more. The prime target group will be the students that run still graphically. There is an option to overclock. Your hardware does cycles. Be raising your CPU voltage you push the CPUs performance. Do this at your own risk!

#### At your own risk - It can damage the RPI
https://hothardware.com/reviews/hot-clocked-pi-raspberry-pi-4-benchmarked-at-214-ghz

https://www.cnx-software.com/2019/07/26/how-to-overclock-raspberry-pi-4/

It is a good idea to get some extra cooling. Gamers can take some fans from their gaming rigs or get a RPI cooler.

### Analog
Your analog input should be continuation or use from earlier work
