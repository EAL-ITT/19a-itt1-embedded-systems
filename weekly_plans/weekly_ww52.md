---
Week: 52
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 52 ITT1 Embedded Systems

## Goals of the week(s)
This is a week free of teaching. Students are advice to work on their topics and use the time to catch up missing information.

### Practical goals

1. Volunteered Self Study 

### Learning goals

1. Volunteered Self Study 


## Deliverable

1. Non

## Schedule

See Time Edit

## Hands-on time

N/A

## Comments
N/A
